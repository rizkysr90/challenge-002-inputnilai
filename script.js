// Rizki Susilo Ramadhan
// Import built-in module readline
const readline = require('readline');
const rl = readline.createInterface({
    // create instance of readline class
    input: process.stdin,
    output: process.stdout,
})

//Color for command line text
const COLOR_GREEN = "\x1b[32m%s\x1b[0m";
const COLOR_YELLOW = "\x1b[33m%s\x1b[0m";
const COLOR_RED = "\x1b[31m%s\x1b[0m";

class ErrorMessageCalculator{
    //Create ErrorMessageCalculator for define an error
    constructor(code,description,message) {
        this.code = code
        this.description = description;
        this.message = message;

    }
    set setError(code) {
        // method setter for set properties in this class
        const format = (code,description,message) => {
            //utility function for set properties in this class
            this.code = code
            this.description = description;
            this.message = message
        }
        if (code === "001") {
            format("001","Invalid Input","Mohon masukkan nilai hanya bilangan saja!")
        } else if (code === "002") {
            format("002","Operator Not Found","Operator yang anda pilih tidak ditemukan")
        }
    }
    get getError() {
        //method getter for display an error message in console
        console.log(COLOR_RED,"==============================================");
        console.log(COLOR_RED,"ERROR MESSAGE");
        console.log(COLOR_RED,"==============================================");
        console.log(`Kode : ${this.code}\nDeskripsi : ${this.description}\nPesan : ${this.message}`);
    }
}

class ScoreCalculator extends ErrorMessageCalculator {
    constructor() {
        super();
        this.trackInput = 0;
        this.arrInput = [];
    
    }
    set setInput(val) {
        this.arrInput[this.trackInput] = Number(val);
        this.trackInput++;
    }
    main() {
        const showResult = () => {
            // Show Result
            // To make color text in the terminal to yellow
            const theme = COLOR_YELLOW;
            // Data
            const nilaiSorted = this.arrInput.sort(this.descendingSort(false));
            const {scorePassed,scoreNotPassed} = this.summarizePassedScore();
            const maxScore = this.maxScore();
            const minScore = this.minScore();
            const averageScore = this.averageScore();

            console.log('=======================');
            console.log('Selesai Input Nilai');
            console.log('=======================');
            console.log(theme,'Data nilai :',nilaiSorted);
            console.log(theme,'Nilai Tertinggi :', maxScore);
            console.log(theme,'Nilai Terendah :', minScore);
            console.log(theme,'Nilai Rata\-rata :',averageScore);
            console.log(theme,'Lulus :',scorePassed);
            console.log(theme,'Tidak Lulus :',scoreNotPassed);
        }

        rl.question(`Nilai ke-${this.trackInput + 1} : `,(answer) => {
            if ( isNaN( Number(answer) )) {
                if (answer === 'q' || answer === 'Q') {
                    if (this.arrInput.length === 0) {
                        console.log('Tidak ada nilai yang dimasukkan');
                        rl.close();
                        return 
                    }
                    showResult();
                    rl.close();
                } 
                else 
                {
                    super.setError = "001"; //invalid input
                    super.getError;
                    rl.close();
                }
            } else {
                this.setInput = answer;
                this.main();
            }
        })
        
    }
    descendingSort(state) {
        if (state) {
            return (a , b) => { 
                if(a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            }
        } else {
            return (a , b) => { 
                if(a > b) {
                    return 1;
                } else if (a < b) {
                    return -1;
                }
                return 0;
            }
        }
    }
    maxScore() {
        return Math.max(...this.arrInput);
    }
    minScore() {
        return Math.min(...this.arrInput);
    }
    averageScore() {
        return this.toFixed(this.arrInput.reduce((prev,current) => {
            return prev + current;
        },0) / this.arrInput.length);
    }
    summarizePassedScore() {
        let scorePassed = 0,scoreNotPassed = 0;
        for (let i = 0; i < this.arrInput.length; i++) {
            if (this.arrInput[i] >= 75) {
                scorePassed++;
            } else {
                scoreNotPassed++;
            }
        }
        return {scorePassed,scoreNotPassed};
    }
    toFixed(val){
        return val % 1 === 0 ? val : Number(val.toFixed(1));
    }
}

function start() {
    console.log(COLOR_GREEN,'Inputkan nilai dan ketik "Q" / "q" jika sudah selesai');
    let scoreCalculator1 = new ScoreCalculator();
    scoreCalculator1.main();
    rl.on('close',() => console.log(`\nTerimakasih,sampai jumpa lagi!👋\nMade with 💖`));
}
start();